package main

import (
	"fmt"
	"math"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"
)

func logAccess(w http.ResponseWriter, r *http.Request, extra string) {
	fmt.Printf("%v: %v %v %v\n", time.Now().Format(time.RFC3339), r.Method, r.URL, extra)
}

func main() {
	runtime.GOMAXPROCS(1)

	var isLivenessSuccess = true
	var isReadinessSuccess = true

	http.HandleFunc("/liveness/setToSucceed", func(w http.ResponseWriter, r *http.Request) {
		isLivenessSuccess = true
		fmt.Fprintf(w, "ok")
		logAccess(w, r, "")
	})

	http.HandleFunc("/liveness/setToFail", func(w http.ResponseWriter, r *http.Request) {
		isLivenessSuccess = false
		fmt.Fprintf(w, "ok")
		logAccess(w, r, "")
	})

	http.HandleFunc("/liveness", func(w http.ResponseWriter, r *http.Request) {
		if isLivenessSuccess {
			w.WriteHeader(200)
			fmt.Fprintf(w, "liveness: 200")
		} else {
			w.WriteHeader(500)
			fmt.Fprintf(w, "liveness: 500")
		}

		logAccess(w, r, "")
	})

	http.HandleFunc("/readiness/setToSucceed", func(w http.ResponseWriter, r *http.Request) {
		isReadinessSuccess = true
		fmt.Fprintf(w, "ok")
		logAccess(w, r, "")
	})

	http.HandleFunc("/readiness/setToFail", func(w http.ResponseWriter, r *http.Request) {
		isReadinessSuccess = false
		fmt.Fprintf(w, "ok")
		logAccess(w, r, "")
	})

	http.HandleFunc("/readiness", func(w http.ResponseWriter, r *http.Request) {
		if isReadinessSuccess {
			w.WriteHeader(200)
			fmt.Fprintf(w, "readiness: 200")
		} else {
			w.WriteHeader(500)
			fmt.Fprintf(w, "readiness: 500")
		}

		logAccess(w, r, "")
	})

	http.HandleFunc("/load", func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		multiple, err := strconv.Atoi(r.URL.Query().Get("multiple"))
		if err != nil || multiple < 0 || multiple > 9999 {
			multiple = 1
		}

		factorStr := r.URL.Query().Get("factor")
		factor, err := strconv.Atoi(factorStr)
		if err != nil || factor < 0 || factor > 9 {
			factor = 1
		}

		max := int(math.Pow10(factor))
		max = multiple * max * max
		for i := 0; i < max; i++ {
			x := 99 * 99
			x = x + 1
		}

		duration := time.Since(start)
		msg := fmt.Sprintf("Done. m: %v, f: %v in %v", multiple, factor, duration)
		fmt.Fprintf(w, msg+"\n")
		logAccess(w, r, msg)
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Ahoy!\n")
		fmt.Fprintf(w, "HOSTNAME: %v\n", os.Getenv("HOSTNAME"))
		fmt.Fprintf(w, "isLivenessSuccess: %v\n", isLivenessSuccess)
		fmt.Fprintf(w, "isReadinessSuccess: %v\n", isReadinessSuccess)
		logAccess(w, r, "")
	})

	// for true {
	// }

	fmt.Println("Server running...")
	http.ListenAndServe(":8080", nil)
}
